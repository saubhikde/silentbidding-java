package silentbidding.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BiddingDetailsBean {

	private String userid;
	private String prod_id;
	private String current_bid_price;
	private String my_bid_price;
	private String step_size;
	private String max_bid_price;
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getProd_id() {
		return prod_id;
	}
	public void setProd_id(String prod_id) {
		this.prod_id = prod_id;
	}
	public String getCurrent_bid_price() {
		return current_bid_price;
	}
	public void setCurrent_bid_price(String current_bid_price) {
		this.current_bid_price = current_bid_price;
	}
	public String getMy_bid_price() {
		return my_bid_price;
	}
	public void setMy_bid_price(String my_bid_price) {
		this.my_bid_price = my_bid_price;
	}
	public String getStep_size() {
		return step_size;
	}
	public void setStep_size(String step_size) {
		this.step_size = step_size;
	}
	public String getMax_bid_price() {
		return max_bid_price;
	}
	public void setMax_bid_price(String max_bid_price) {
		this.max_bid_price = max_bid_price;
	}
	
	
	
	
	
	
}
