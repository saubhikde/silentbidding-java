package silentbidding.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FavouriteBean {
	
	private String user_id;
	private String prod_id;
	private String pro_name;
	
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getProd_id() {
		return prod_id;
	}
	public void setProd_id(String prod_id) {
		this.prod_id = prod_id;
	}
	public String getPro_name() {
		return pro_name;
	}
	public void setPro_name(String pro_name) {
		this.pro_name = pro_name;
	}
	
	

}
