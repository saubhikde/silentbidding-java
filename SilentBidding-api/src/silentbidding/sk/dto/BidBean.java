package silentbidding.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BidBean {
	
	private String userid;
	private String productid;
	private String curr_bid_Price;
	private String date_time;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getProductid() {
		return productid;
	}
	public void setProductid(String productid) {
		this.productid = productid;
	}
	public String getCurr_bid_Price() {
		return curr_bid_Price;
	}
	public void setCurr_bid_Price(String curr_bid_Price) {
		this.curr_bid_Price = curr_bid_Price;
	}
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	
	
	

}
