package silentbidding.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import silentbidding.sk.utility.Database;



public class UserLoginDao {

	public static boolean userLogin(String uname,String password) throws Exception{
		Boolean findUser = false;
		
		
		boolean insertStatus = false;
		Connection dbConn = null;
		String id = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("select count(*) from user_master where user_id= ? and password= ?");
			ps.setString(1,uname);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			int	idInt= rs.getInt(1);
			
			if(idInt> 0){
				findUser = true;
			}
				
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	
		
		
		return findUser;
	}
	
	public static String getUserId(String uname,String password) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		String id = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("SELECT id FROM user_master WHERE user_id=? and password = ? ");
			ps.setString(1,uname);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			int	idInt= rs.getInt("id");
			
			id= String.valueOf(idInt);
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return id;
	}
	
	
	
	public static boolean updateDevice(String device_id,String password,String user_name)  throws Exception{
		Connection dbConn = null;
		boolean	updateStatus = false;
		System.out.println("In Register Dao");
		try {
				String ve = "";
			
				Database database = new Database();
				dbConn = database.Get_Connection();
				
			
			String query = " UPDATE user_master SET device_id = '"+device_id+"' WHERE password= '"+password+"' and user_id = '"+user_name+"'";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
			 updateStatus = true;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		}  finally {
			if (dbConn != null) {
				dbConn.close();
			//	dbConn.commit();
			}
		}
		return updateStatus;
	}
	
	
}
