package silentbidding.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import silentbidding.sk.utility.Database;



public class FavProductFetchDao {
		
	public static JSONArray GetFeeds(String uid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONArray jsonArray = new JSONArray();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			
        	String query = "select f.user_id,p.id,p.prod_name,p.short_description,p.long_description,p.image,p.start_time,p.end_time,p.status,p.product_type,p.min_bid_price,p.max_bid_price from fav_master f,product p where f.prod_id = p.id and f.user_id= '"+uid+"'" ;
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            int total_rows = rs.getMetaData().getColumnCount();
		            JSONObject obj = new JSONObject();
		            for (int i = 0; i < total_rows; i++) {
		            	Object obj1 = new Object();
		            	if(rs.getObject(i+1)==null){
		            	obj1 = "";
		            	}else{
		            		obj1 = rs.getObject(i+1);
		            	}
		                obj.put(rs.getMetaData().getColumnLabel(i + 1)
		                        .toLowerCase(), obj1);
		            }
		            jsonArray.put(obj);
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return jsonArray;
	}
	
}
