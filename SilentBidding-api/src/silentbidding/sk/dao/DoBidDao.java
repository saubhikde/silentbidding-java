package silentbidding.sk.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import silentbidding.sk.utility.Database;



public class DoBidDao {
	public boolean bidExistance(int pid,int uid,double cbp) throws Exception{
		boolean status = false;
		Connection conn = null;
		Database database = new Database();
		conn = database.Get_Connection();
		
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try{
	    		if(conn!=null){
	    				
	    			String checkFlagExistence  = "select count(id) from bidding_master where prod_id= ? and current_bid_price = ?";
	    				pstmt = conn.prepareStatement(checkFlagExistence);
	    				
	    				
	    				pstmt.setInt(1, pid);
	    				
	    				pstmt.setDouble(2,cbp);
	    				
	    				
	    				rs = pstmt.executeQuery();
	    				while(rs.next()){
	    					if(rs.getInt(1) != 0){
	    						status = true;
	    						return status;
	    					}
	    				}
	    		}		
	    }catch(Exception e){
	    	e.printStackTrace();
	    }finally{
	    	rs.close();
	    	pstmt.close();
	    	conn.close();
	    
	    }
		return status;
				
	}

	public static boolean insertBid(int pid,int  uid,double cbp,Date c_date_time) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			
			String query = "INSERT into bidding_master(prod_id,user_id, current_bid_price,bid_time) values('"+pid+ "',"+"'"
					+ uid + "','" + cbp + "','" + c_date_time + "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
				insertStatus = true;
			}
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return insertStatus;
	}

	public double fetchMaxPrice(int pid) throws Exception{
		double price = 0.00;
		
		Connection connection = null;
		
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			
        	String query = "select id,max_bid_price from product where id ="+pid ;
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		          price = rs.getDouble(2);
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		
		
		return price;
	}
	
	
	public boolean updateStatus(int pid)  throws Exception{
		Connection dbConn = null;
		boolean	updateStatus = false;
		System.out.println("In Register Dao");
		try {
				String ve = "";
			
				Database database = new Database();
				dbConn = database.Get_Connection();
				
			
			String query = " UPDATE product SET status = 'closed' WHERE id = "+pid;
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
			 updateStatus = true;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		}  finally {
			if (dbConn != null) {
				dbConn.close();
			//	dbConn.commit();
			}
		}
		return updateStatus;
	}
	
	


}
