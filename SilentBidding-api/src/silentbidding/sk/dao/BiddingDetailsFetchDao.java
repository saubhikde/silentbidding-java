package silentbidding.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.codehaus.jettison.json.JSONObject;

import silentbidding.sk.utility.Database;



public class BiddingDetailsFetchDao {

	public double GetFeedsMaxBidPrice(int pid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONObject obj = new JSONObject();
		double maxbid = 0.00;
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			String query = "select max(current_bid_price) from bidding_master where prod_id = "+pid;
        	
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            
		        	
		        	maxbid = rs.getDouble("max");
		            
		            
		           
		            
		           
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return maxbid;
	}
	
	public double GetFeedsMyBid(int uid,int pid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONObject obj = new JSONObject();
	
		double mybid = 0.00;
		
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
	
			
			String query = "select max(current_bid_price) from bidding_master where user_id = "+ uid +" and prod_id = "+pid;
        	
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            
		        	
		        	mybid = rs.getDouble("max");
		            
		           
		           
		            
		           
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return mybid;
	}
	
	public JSONObject GetFeedsBidDetails(int pid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONObject obj = new JSONObject();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			String query = "select min_bid_price,step_size,max_bid_price from product where id = "+pid;
        	
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            
		        	
		            double min_bid = rs.getDouble("min_bid_price");
		            String step =  rs.getString("step_size");
		            double max_bid = rs.getDouble("max_bid_price");
		            
		            obj.put("min_bid_price",min_bid);
		            obj.put("step_size",step);
		            obj.put("max_bid_price",max_bid);
		           
		            
		           
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return obj;
	}
}
