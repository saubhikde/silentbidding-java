package silentbidding.sk.webservice;




import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import silentbidding.sk.dao.BiddingDetailsFetchDao;
import silentbidding.sk.dto.BiddingDetailsBean;





import com.sun.jersey.multipart.FormDataParam;

@Path("/bidfetch")
public class BiddingDetailsFetch {
	@POST
	// Path: http://localhost/<appln-folder-name>/login/dologin
	@Path("/dobidfetch")
	@Consumes(MediaType.APPLICATION_JSON)
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/login/dologin?username=abc&password=xyz
	public JSONObject doLogin(BiddingDetailsBean biddingDetailsBean) throws Exception{
		JSONObject obj1  = new JSONObject();
		System.out.println("This is here");
		try 
		{
			String uid = biddingDetailsBean.getUserid();

			String pid = biddingDetailsBean.getProd_id();
			
			System.out.println(uid);
			System.out.println(pid);
			
			int int_uid = Integer.parseInt(uid);
			int int_pid = Integer.parseInt(pid);
			
			
			//
			
			/////////////////////////////////////////////////////
			BiddingDetailsFetchDao biddingDetailsFetchDao = new BiddingDetailsFetchDao();
			
			
			double curr_bid = biddingDetailsFetchDao.GetFeedsMaxBidPrice(int_pid);
			double my_bid =		biddingDetailsFetchDao.GetFeedsMyBid(int_uid, int_pid);
			
			
				
				
				obj1 = biddingDetailsFetchDao.GetFeedsBidDetails(int_pid);
				
				obj1.put("current_bid_price", curr_bid);
				obj1.put("my_bid_price", my_bid);
				
				
				
				
				
				
			
			
			
			/////////////////////////////////////////////////////

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("error");
		}
		return obj1;		
	}
}
