package silentbidding.sk.webservice;



import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import silentbidding.sk.dao.UserLoginDao;
import silentbidding.sk.dto.UserBean;
import silentbidding.sk.utility.Utitlity;

import com.sun.jersey.multipart.FormDataParam;








/**
 * @SaurabhKar
 *
 */
@Path("/login")
public class Login {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/dologin")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doLogin(UserBean userBean) throws SQLException, Exception{
			String response = "";
			
			
			String uname = userBean.getUser_id();
			String password = userBean.getPassword();
			String device_id = userBean.getDevice_id();
					
			
			
			System.out.println("Inside doLogin ::"+uname+" * "+" * "+password+"  *  "+device_id);
			int retCode = registerUser(uname,password,device_id);
			if(retCode == 0){
				
				String uid=UserLoginDao.getUserId(uname, password);
				
				UserLoginDao.updateDevice(device_id, password, uname);
				
				response = Utitlity.constructJSON("login successful",true,uid,"Success");
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
					response = Utitlity.constructJSON("login error",false);
			}
			return response;
					
		}
		
		private int registerUser(String uname,String password,String dev_id) throws Exception{
			//System.out.println("Inside doLogin **::"+uname+" * "+" * "+phone+"  *  "+email+" * "+add+"  *  "+city+"  *  "+pin);
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(uname) && Utitlity.isNotNull(password) && Utitlity.isNotNull(dev_id)){
				try {
					if(UserLoginDao.userLogin(uname, password)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				} 
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}

