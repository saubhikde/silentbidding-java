package silentbidding.sk.webservice;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import silentbidding.sk.dao.DoBidDao;
import silentbidding.sk.dto.BidBean;
import silentbidding.sk.utility.Utitlity;







/**
 * @SaurabhKar
 *
 */
@Path("/bid")
public class Bid {
	// HTTP Get Method
	@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/dobid")  
		//consumes json as a request
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doBidHere(BidBean bidBean) throws Exception{
			String response = "";
			//System.out.println("Inside doLogin ::"+strname+" * "+" * "+uname+"  *  "+pwd+" * "+mobno+"  *  "+name);
			
			String uid = bidBean.getUserid();
			String pid = bidBean.getProductid();
			String cbp = bidBean.getCurr_bid_Price();
			String date_time = bidBean.getDate_time();
			
			int retCode = 4;
			DoBidDao doBidDao = new DoBidDao();
			Boolean insertStatus = doBidDao.bidExistance(Integer.parseInt(pid), Integer.parseInt(uid),Double.parseDouble(cbp));
			
			if(insertStatus == false){
			   double mprice = doBidDao.fetchMaxPrice(Integer.parseInt(pid));
				if(Double.parseDouble(cbp) == mprice){
					retCode = registerUser(uid,pid, cbp,date_time);
					doBidDao.updateStatus(Integer.parseInt(pid));
					
				}else{
				retCode = registerUser(uid,pid, cbp,date_time);
				}
			}else{
				retCode = 5;
				System.out.println("already bid someone");
				
			}
			
		    
			if(retCode == 0){
				response = Utitlity.constructJSON("BID INCREASED",true);
			}else if(retCode == 3){
				response = Utitlity.constructJSON("NOT SUCCESS",false, "Error occured");
			}else if(retCode == 5){
				response = Utitlity.constructJSON("ALREADY SOMEONE BID THIS AMOUNT",false);
			}
			return response;
					
		}
		
		private int registerUser(String uid, String pid, String cbp,String date_time){
		//	System.out.println("Inside doLogin **::"+str_name+" * "+" * "+uname+"  *  "+pwd+" * "+mob_no+"  *  "+name);
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(uid) && Utitlity.isNotNull(pid) && Utitlity.isNotNull(cbp) && Utitlity.isNotNull(date_time)){
				try {
					
					
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
					
					Date date = formatter.parse(date_time);
					
					
						if(DoBidDao.insertBid(Integer.parseInt(pid),Integer.parseInt(uid), Double.parseDouble(cbp), new java.sql.Date(date.getTime()))){
							System.out.println("RegisterUSer if");
							result = 0;
						}
					
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
		
	
}

